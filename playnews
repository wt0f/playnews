#!/bin/bash
#
# playnews v0.11 - WA3DSP 06/2017
#
# Script to download ARRL News or ARN and divide into 2.5 minute segments with breaks.
# Includes voice messages before play, at breaks, and after play.
# This script can be configured for global playback! 
# DO NOT run this on a multi-node connected circuit without consideration. 
# Change MODE to localplay for strictly local node play.
#
# This code is written to work on the hamvoip.org BBB/RPi2 Allstar releases
# All required packages are pre-installed on those systems.
# 
# For proper operation holdofftelem in rpt.conf should either not be defined or
# defined as =0  This stops keyups from delaying the news during its playing
#
# You can run this script from a cron job or from the command line at least
# 15 minutes before the defined run time (TIME value) but it can be scheduled
# anytime within 24 hours prior to the run time.
#
# cron example -
#
# Prime news for play every Tuesday at 8:30PM - actual playtime set by defined 
# comand line TIME parameter. If Playtime is 9PM (2100) 
# This would send pre warnings at 8:50 and 8:55 PM. 
#
# Start a cron job every tuesday at 8:30 PM to run at 9 PM the same day
# and play ARRL news on node 40000, globally
#
# 30 20 * * 2 /etc/asterisk/playnews ARRL 21:00 40000 G &> /dev/null 2>&1
#
# Play ARN news on Thursday at 7PM on node 40000, Start playnews at 6 PM, Play locally
#
# 00 18 * * 4 /etc/asterisk/playnews ARN 19:00 40000 L &> /dev/null 2>&1

# The audio files ARRLstart5, ARRLstart10, ARRLstart, ARRLcontinue, ARRLstop
# and ARNstart, ARNstart10, ARNstart, ARNcontinue, ARNstop
# are supplied but could be customized for your needs. The audio
# files must be in the directory defined by VOICEDIR
#
# ARRLstart10 or ARNstart10   - voice message at ten minutes before start
# ARRLstart5 or ARNstart5     - voice message at five minutes before start
# ARRLstart or ARNstart       - voice message at start of play
# ARRLcontinue or ARNcontinue - voice message at breaks
# ARRLstop or ARNstop         - voice message at end of play
#
# V0.11 - Changed to separate playnews.ini file to specify
# URL's
#
# V0.10 - changeed URL's
#
# V0.9 update
#
#   - Changed ARRL URL to content.blubrry.com
#
# v0.8 update
#
#   - added check for downloaded MP3 filesize. Playnews will not
#     play if size is less than 100K. On holidays ARRL only has
#     small html file that gets downloaded. 
#
# v0.7 updates
#
#   - Added TMPDIR user settable variable
#
# v0.6 updates
#   - Now requires that all parameters be entered
#     except mode which defualts to global
#   - More parameter checking added
#   - Time can be set to "NOW" for immediate start
#
#   Command line format - playnews ARRL|ARN 21:00|NOW 40000 L|G
#   Options are ARRL or ARN news, specific 24 hour time or "NOW"
#   and local "L" or Global "G" play modes.
#
#   DO NOT use the the "NOW" time parameter in a cron !!!

script_dir=$(dirname "$0")
if [ ! -f "$script_dir/playnews.ini" ]
     then
        echo -e "\nplaynews.ini file missing\n"
        exit
     else
        source "$script_dir/playnews.ini"
fi

# The following variable needs to be set if different for your install
#
# VOICEDIR - Directory for playnews voice files
# Usually in the same directory as the playnews script

VOICEDIR="/etc/asterisk/local/playnews"

# TMPDIR - Directory for temporary file storage
# Note if at all possible this should not be on the SD card.
# Use of /tmp or a USB mounted stick is preferred
# Note that the BBB may not have enough memory in /tmp to process

TMPDIR="/tmp" 

# End User defines

if [ ! -f $VOICEDIR/ARNstart ]
  then
    echo "playnews voice files not found - check VOICEDIR in script"
    exit 1
fi

# NEWSTYPE is either ARRL or ARN, Always required as parameter 1
#
if [ -z "$1" ]
  then
     echo "No Play type given - ARN or ARRL"
     exit 1
  else
     NEWSTYPE=${1^^}
     if [ "$NEWSTYPE" != "ARN" ] && [ "$NEWSTYPE" != "ARRL" ]
        then 
           echo "Play type must be ARRL or ARN"
           exit 1
     fi
fi

# Node number to play on or from
#
if [[ ! $3 =~ ^-?[0-9]+$ ]]
  then
    echo "Node number required" 
    exit 1
  else
     NODE=$3
fi

# Mode - 'localplay' or 'playback' (global)
# 'localplay' only plays on the node specified
# 'playback' plays on the specified node and ALL nodes connected to it
#
if [ -z $4 ]
   then 
     MODE="playback"
   elif
     [ ${4^^} == "L" ]
       then
         MODE="localplay"
   elif
     [ ${4^^} == "G" ]
       then
         MODE="playback"
   else
     echo "Wrong mode type - L for Local play, G or null for global play"
     exit 1
fi

if [ $MODE == "playback" ]
   then
     MODETYPE="(global)"
   else
     MODETYPE="(local)"
fi

# Time to start - 24 hour time - required 2nd command line parameter
# Time example 03:19 = 3:19 AM, 22:45 = 10:45 PM 
#
if [ ${2^^} != "NOW" ] && [[ !  $2 =~ ^[0-9][0-9]:[0-9][0-9]$ ]]
  then
    echo "No Time supplied - Enter 24 hour time to play as 00:00 - (7 PM = 19:00)"
    exit 1
fi
TIME=$2

# Download Newsline or ARRL  and convert to wav
rm -f $TMPDIR/news.mp3
echo "Starting Download of $NEWSTYPE Audio News @ $(date +%H:%M:%S)"

# ARN_URL and ARRL_URL are set in playnews.ini
if [ $NEWSTYPE == "ARN" ]
  then 
# Timeout set to 15 seconds with 4 retries. Set the timeout longer for slow circuits.
     wget -T 15 --tries=4 -O $TMPDIR/news.mp3 $ARN_URL 
  else
# not ARN so get ARRL
     wget -T 15 --tries=4 -O $TMPDIR/news.mp3 $ARRL_URL
fi

if [ -f $TMPDIR/news.mp3 ];
 then
 echo "Download complete @ $(date +%H:%M:%S)"
else
 echo "Download failed"
 exit 0
fi

# On holidays ARRL does not put out a news MP3. Instead there is a 
# shorter HTML file. The following routine checks for this and
# exits if there is no valid MP3 file.

filesize=$(wc -c <$TMPDIR/news.mp3)
if [ $filesize -lt 100000 ]
 then
    echo "File size too small for play"
    exit 0
fi

echo "Converting from MP3 to individual ulaw format files"

lame -h --decode $TMPDIR/news.mp3 $TMPDIR/news2.wav &> /dev/null 2>&1
# Get the length of the file in seconds
LENGTH=`soxi -D $TMPDIR/news2.wav`
#echo $LENGTH
START=0
# 2.5 minute = 150 seconds
INCREMENT=150
# Calculate number of segments
MAXPART=`echo $LENGTH/$INCREMENT | bc` 
let "MAXPART += 1"
PART="1"
#echo $MAXPART

# Divide into 2.5 minute segments
while [ "$PART" -le "$MAXPART" ]; do
      sox --temp $TMPDIR $TMPDIR/news2.wav $TMPDIR/news.part$PART.wav trim $START $INCREMENT &> /dev/null 2>&1
      echo "Creating $TMPDIR/news.part$PART.wav"
      INCREMENT=155
      START=$(($START-5+$INCREMENT))
#     echo "$PART - $START - $INCREMENT  $(($START+$INCREMENT))"
      let "PART += 1"
done

# Convert each segment to ulaw
PART="1"
while [ "$PART" -le "$MAXPART" ]; do
      sox --temp $TMPDIR -V $TMPDIR/news.part$PART.wav -r 8000 -c 1 -t ul $TMPDIR/news.part$PART.ul &> /dev/null 2>&1
      echo "Converting to $TMPDIR/news.part$PART.ul"
      rm $TMPDIR/news.part$PART.wav
      let "PART += 1"
done

if [ ${TIME^^} != "NOW" ]
then

echo "$NEWSTYPE news will start at $TIME and use $MODE $MODETYPE mode on"
echo "node - $NODE  with 5 and 10 minute pre-warnings"
 
# Last warning time - 5 minutes before
TIME5=`date --date "$TIME now 5 minutes ago" +%H:%M`
# First warning time - 10 minutes before
TIME10=`date --date "$TIME now 10 minutes ago" +%H:%M`

# Wait and Send 10 minute announcement
echo "Waiting to send 10 minute warning"
while [ $(date +%H:%M) != $TIME10 ]; do sleep 1; done
   # Start 10 minute message, add 3 second delay to beginning
   cat $VOICEDIR/silence3.ul "$VOICEDIR/${NEWSTYPE}start10.ul" > $TMPDIR/news.ul
   /usr/bin/asterisk -rx "rpt $MODE $NODE $TMPDIR/news"

# Wait Send 5 minute announcement
echo "Waiting to send 5 minute warning"
while [ $(date +%H:%M) != $TIME5 ]; do sleep 1; done
   # Start 5 minute message, add 3 second delay to beginning
   cat $VOICEDIR/silence3.ul "$VOICEDIR/${NEWSTYPE}start5.ul" > $TMPDIR/news.ul
   /usr/bin/asterisk -rx "rpt $MODE $NODE $TMPDIR/news"

# wait for start time
echo "Waiting for start time"
while [ $(date +%H:%M) != $TIME ]; do sleep 1; done

else

echo "$NEWSTYPE news will start $TIME and use $MODE $MODETYPE mode on node - $NODE"
echo -n "Press any key to start news..."
read -n 1 

fi

# send ID
/usr/bin/asterisk -rx "rpt fun $NODE *80"
echo
echo "Starting in 10 seconds - Hit Ctrl C to interrupt"
for (( X=10; X >= 1; X-- ))
do
     echo -n "$X "
     sleep 1
done
echo

# send start announcment
# Add 3 second delay to beginning of text
cat $VOICEDIR/silence3.ul "$VOICEDIR/${NEWSTYPE}start.ul" > $TMPDIR/news.ul
/usr/bin/asterisk -rx "rpt $MODE $NODE $TMPDIR/news"

sleep 10

# Start news
PART="1"
while [ "$PART" -le "$MAXPART" ]; do
      echo "Playing file $TMPDIR/news.part$PART.ul"
      if [ "$PART" -lt  "$MAXPART" ]; then 
         # Add Station break message with 1 second delay
         cat $TMPDIR/news.part$PART.ul $VOICEDIR/silence1.ul "$VOICEDIR/${NEWSTYPE}continue.ul" > $TMPDIR/news.ul  
         /usr/bin/asterisk -rx "rpt $MODE $NODE $TMPDIR/news"
         # Break every 2.5 minutes for 20 seconds
         # Note this break has to be long enough for network ID's
         sleep 175
      else
         # Append end message with 2 second delay
         cat $TMPDIR/news.part$PART.ul $VOICEDIR/silence2.ul "$VOICEDIR/${NEWSTYPE}stop.ul" > $TMPDIR/news.ul
         /usr/bin/asterisk -rx "rpt $MODE $NODE $TMPDIR/news"
         # Terminate script at end of audio
         LENGTH=`soxi -D $TMPDIR/news.ul 2>/dev/null`
         DELAY=${LENGTH/.*} 
         sleep $DELAY
      fi
      let "PART += 1"
done

# Remove all files created by playnews
rm -f $TMPDIR/news*

# Done
exit 0


