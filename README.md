playnews script for AllStar systems
===================================

New version of the `playnews` script for Allstar Link and other computer
radio gateway systems that use Asterisk as the audio interface. It is a
rewrite of the original BASH script into Python. Features of this rewrite
are:

- Split the audio file upon silence
- Ignore short duration segments caused by separation tone
- Support more than just ARN and ARRL news feeds
- Keep track of how long since last station identifier and
  schedule the identifier in between audio segments
- Play cached audio segments until configurable expiration time
- Download and split audio segments without having to play them


Requirements
------------

- ffmpeg
- Python 3.6 or greater

For the `ffmpeg` requirement, installation from the systems package manager
should be more than sufficient. For Debian/Ubuntu based systems this would be
`apt install ffmpeg`. For Arch based systems (HamVOIP) `ffmpeg` can be
installed with `pacman -Sy ffmpeg`.

For HamVOIP systems the installation of Python is not as clear cut. Currently
the package repositories for the HamVOIP distributions are still distributing
Python that is almost 7 years old. Development of the `playnews` script
was done with Python 3.7.12 which can be compiled from source. This will take
a number of hours to complete, but it is one approach. It is planned to
create a Arch package of Python 3.7 that one could download and install
without having to go through a complete compile.

Until an Arch package is available, one would need to issue the following
commands to build and install Python 3.7

    curl -L -o python.tar.gz https://www.python.org/ftp/python/3.7.12/Python-3.7.12.tgz
    tar xzf python.tar.gz
    cd Python-3.7.12
    ./configure --prefix=/usr --enable-optimizations
    make
    sudo make install

One thing to be aware of is that installation of a new version of Python
may affect other software installed. Most notibly this occurs with the
`RedNode` software installed on ClearNodes. After installing Python 3.7,
One would need to install the required Python modules to support `RedNode`.
This can be done with `pip3 install -r requirements-rednode.txt` in the
`playnews` source directory. After this is done, one should consider
rebooting the ClearNode.

Installation
------------

Once the requirements have been satisfied, the installation of `playnews`
is nothing more than creating a directory and copying the `playnews` source
files to the directory.

After the source files are in place it is just a matter of adding an entry
to `rpt.conf` to invoke the `playnews` script. Something like the following
is all that is needed.

    851=cmd,/usr/local/playnews/playnews.py ARRL

There are entries in `playnews.ini` for both the ARRL and Amateur Radio
Newsline feeds. `playnews` is not limited to these two audio feeds and
can pretty much take any audio news feed.

Original README file
--------------------

The following is the original information from the `playnews` script that
is distributed in the HamVOIP distribution. It is provided here for
reference and at some point will be removed from this `README` file.

```text
# playnews v0.11 - WA3DSP 06/2017
#
# Script to download ARRL News or ARN and divide into 2.5 minute segments with breaks.
# Includes voice messages before play, at breaks, and after play.
# This script can be configured for global playback!
# DO NOT run this on a multi-node connected circuit without consideration.
# Change MODE to localplay for strictly local node play.
#
# This code is written to work on the hamvoip.org BBB/RPi2 Allstar releases
# All required packages are pre-installed on those systems.
#
# For proper operation holdofftelem in rpt.conf should either not be defined or
# defined as =0  This stops keyups from delaying the news during its playing
#
# You can run this script from a cron job or from the command line at least
# 15 minutes before the defined run time (TIME value) but it can be scheduled
# anytime within 24 hours prior to the run time.
#
# cron example -
#
# Prime news for play every Tuesday at 8:30PM - actual playtime set by defined
# comand line TIME parameter. If Playtime is 9PM (2100)
# This would send pre warnings at 8:50 and 8:55 PM.
#
# Start a cron job every tuesday at 8:30 PM to run at 9 PM the same day
# and play ARRL news on node 40000, globally
#
# 30 20 * * 2 /etc/asterisk/playnews ARRL 21:00 40000 G &> /dev/null 2>&1
#
# Play ARN news on Thursday at 7PM on node 40000, Start playnews at 6 PM, Play locally
#
# 00 18 * * 4 /etc/asterisk/playnews ARN 19:00 40000 L &> /dev/null 2>&1

# The audio files ARRLstart5, ARRLstart10, ARRLstart, ARRLcontinue, ARRLstop
# and ARNstart, ARNstart10, ARNstart, ARNcontinue, ARNstop
# are supplied but could be customized for your needs. The audio
# files must be in the directory defined by VOICEDIR
#
# ARRLstart10 or ARNstart10   - voice message at ten minutes before start
# ARRLstart5 or ARNstart5     - voice message at five minutes before start
# ARRLstart or ARNstart       - voice message at start of play
# ARRLcontinue or ARNcontinue - voice message at breaks
# ARRLstop or ARNstop         - voice message at end of play
#
# V0.11 - Changed to separate playnews.ini file to specify
# URL's
#
# V0.10 - Again changed URL's
#
#
# V0.9 update
#
#   - Changed ARRL URL to blubrry.com
#
# v0.8 update
#
#   - added check for downloaded MP3 filesize. Playnews will not
#     play if size is less than 100K. On holidays ARRL only has
#     small html file that gets downloaded.
#
# v0.7 updates
#
#   - Added TMPDIR user settable variable
#
# v0.6 updates
#   - Now requires that all parameters be entered
#     except mode which defualts to global
#   - More parameter checking added
#   - Time can be set to "NOW" for immediate start
#
#   Command line format - playnews ARRL|ARN 21:00|NOW 40000 L|G
#   Options are ARRL or ARN news, specific 24 hour time or "NOW"
#   and local "L" or Global "G" play modes.
#
#   DO NOT use the the "NOW" time parameter in a cron !!!
```
