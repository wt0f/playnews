#!/usr/bin/env python3

import os
import sys
import re
import requests
import logging
import arrow
import time
import shutil

from configparser import ConfigParser, ExtendedInterpolation
from argparse import ArgumentParser
from tempfile import _get_candidate_names as temp_filename
from dateutil.parser import parse as date_parse
from datetime import datetime, timedelta
from pydub import AudioSegment
from pydub.silence import split_on_silence

import pdb
from pprint import pprint

logging_format = "%(levelname)-5s %(module)s:%(funcName)s %(message)s"
logging.basicConfig(filename="", format=logging_format, level=logging.WARNING)

MIN_PYTHON = (3, 1)
if sys.version_info < MIN_PYTHON:
    sys.exit("Python %s.%s or later is required.\n" % MIN_PYTHON)

class ConfSettings:
    def __init__(self, settings):
        self._data = settings

    def value(self, key, default):
        if key in self._data:
            return self._data[key]
        return default

    def keys(self):
        return self._data.keys()

    def __getitem__(self, key):
        return self.value(key, None)

    def __contains__(self, s):
        return s in self.keys()

def file_extension(filename):
    return os.path.splitext(filename)[1]

def fetch_audio_file(url, base_filename, verify=True):
    logging.info("Downloading {}".format(url))

    # may need to set a retry strategy of 4 retries
    r = requests.get(url=url, timeout=(5,15), verify=verify)

    # TODO update comment
    # On holidays ARRL does not put out a news MP3. Instead there is a
    # shorter HTML file. The following routine checks for this and
    # exits if there is no valid MP3 file.
    if len(r.content) < 100000:
        logging.error("Downloaded file is too small")
        return None

    ext = file_extension(url)
    download_filename = base_filename+'.'+ext

    logging.debug("Writing audio file to {}".format(download_filename))
    with open(download_filename, 'wb') as fp:
        fp.write(r.content)
    return download_filename

def feed_has_expired(nugget, play_dir, expiration):
    now = arrow.now()

    manifest_file = os.path.join(play_dir, "{}-manifest.txt".format(nugget))
    if os.path.exists(manifest_file):
        create_time = arrow.get(os.stat(manifest_file).st_ctime)
        if create_time.dehumanize('in {}'.format(expiration)) < now:
            # cached files have expired
            return True
        else:
            return False
    return True

def find_date(date_str, date_fmt):
    # arrow module numbers its day of the week oddly
    dow = {'sunday':6, 'monday':0, 'tuesday':1, 'wednesday':2,
           'thursday':3, 'friday':4, 'saturday':5}

    # process day of week dates
    if date_str.lower() in dow.keys():
        now = arrow.now()
        # find next day of week occuring
        next_date = arrow.util.next_weekday(now, dow[date_str.lower()])
        # subtract 1 week from the date
        target_date = arrow.get(next_date).dehumanize('7 days ago')
        return target_date.format(date_fmt)

def find_audio_stream(feed_name, conf):
    logging.debug("Building URL for {} feed".format(feed_name))
    if not conf.has_section(feed_name):
        logging.error("Unable to find audio feed for {}".format(feed_name))
        return None

    url = conf[feed_name]['url']

    # Look for keywords to substitute
    while True:
        m = re.search(r'%(\w+)%', url)
        if m == None:
            break

        kw = m.group(1)
        if kw == 'DATE':
            date_fmt = conf[feed_name]['date_format'].strip(r'\'"')
            date_str = conf[feed_name]['date'].strip(r'\'"')
            replace_str = find_date(date_str, date_fmt)

        url = re.sub(r'%{}%'.format(kw), replace_str, url)


    return url

def is_ffmpeg_installed():
    return check_path('ffmpeg') != None

def is_sox_installed():
    return check_path('sox') != None

def check_path(exec_name):
    for dir in os.environ['PATH'].split(':'):
        if os.path.exists(os.path.join(dir, exec_name)):
            return os.path.join(dir, exec_name)
    return None

def play_mode(conf, feed_conf):
    mode = 'localplay'
    if 'mode' in conf:
        mode = conf['mode']
    if 'mode' in feed_conf.keys():
        mode = feed_conf['mode']
    return mode

def asterisk_prog(conf):
    return 'asterisk_prog' in conf and conf['asterisk_prog'] or '/usr/bin/asterisk'

def node_num(num, conf):
    return 'node' in conf and conf['node'] or num

def id_time(conf, default=480):
    return 'id_time' in conf and conf['id_time'] or default

# Define a function to normalize a chunk to a target amplitude.
def match_target_amplitude(aChunk, target_dBFS):
    ''' Normalize given audio chunk '''
    change_in_dBFS = target_dBFS - aChunk.dBFS
    return aChunk.apply_gain(change_in_dBFS)

def split_audio_file(audio_file, dest_dir='.', nugget='unknown',
                     split_type='time', silence_duration=2.0,
                     silence_thresh=-16, ignore_short=0):
    logging.debug("split_audio_file({}, dest_dir={}, nugget={}, split_type={}, silence_duration={}, silence_thresh={}, ignore_short={}"
            .format(audio_file, dest_dir, nugget, split_type, silence_duration,
                    silence_thresh, ignore_short))

    # Load your audio.
    logging.debug("Loading audio from {}".format(audio_file))
    ext = file_extension(audio_file)
    audio = AudioSegment.from_file(audio_file, format=ext[1:])
    dBFS = audio.dBFS

    # Split track where the silence is 2 seconds or more and get chunks using
    # the imported function.
    logging.info("Scanning audio file for periods of silence")
    logging.debug("silence_duration={}  silence_thresh={}".format(int(float(silence_duration) * 1000), silence_thresh))
    chunks = split_on_silence (
        # Use the loaded audio.
        audio,
        # Specify that a silent chunk must be at least 2 seconds or 2000 ms long.
        min_silence_len = int(float(silence_duration) * 1000),
        # Consider a chunk silent if it's quieter than -16 dBFS.
        # (You may want to adjust this parameter.)
        silence_thresh = dBFS + int(silence_thresh),
        seek_step=1000
    )

    manifest = os.path.join(dest_dir, "{}-manifest.txt".format(nugget))
    with open(manifest, 'w') as fp:
        # Process each chunk with your parameters
        for i, chunk in enumerate(chunks):
            chunk_num = "%02d" % i
            # Create a silence chunk that's 0.5 seconds (or 500 ms) long for padding.
            silence_chunk = AudioSegment.silent(duration=500)

            # Add the padding chunk to beginning and end of the entire chunk.
            audio_chunk = silence_chunk + chunk + silence_chunk

            # Normalize the entire chunk.
            normalized_chunk = match_target_amplitude(audio_chunk, -20.0)
            mono_chunk = normalized_chunk.split_to_mono()[0]
            chunk_duration = mono_chunk.duration_seconds

            if chunk_duration > float(ignore_short):
                # Export the audio chunk as a wav file.
                base_filename = "{}{}".format(nugget, chunk_num)
                logging.info("Exporting {}.wav".format(base_filename))
                mono_chunk.set_channels(1)
                mono_chunk.export(
                    os.path.join(dest_dir, "{}.wav".format(base_filename)),
                    format = "wav", bitrate="64k", parameters=['-ar', '8000'])

                # convert the wav file to a ulaw file
                os.system("sox {}.wav {}.ul".format(
                        os.path.join(dest_dir, base_filename),
                        os.path.join(dest_dir, base_filename)))
                os.unlink(os.path.join(dest_dir, "{}.wav".format(base_filename)))

                # Record the chunk in the manifest
                fp.write("{}.ul,{:.2f}\n".format(base_filename,
                         mono_chunk.duration_seconds))
            else:
                logging.info("Discarding short segment: {} secs".format(chunk_duration))

def asterisk_play(audio_file, asterisk_prog='/usr/bin/asterisk',
                  mode='localplay', node_num=None):
    logging.debug("asterisk_play({}, asterisk_prog={}, mode={}, node_num={}".format(audio_file, asterisk_prog, mode, node_num))

    #/usr/bin/asterisk -rx "rpt $MODE $NODE $TMPDIR/news"
    os.system('{} -rx "rpt {} {} {}"'.format(asterisk_prog, mode,
                node_num, audio_file))

def send_station_id(text):
    pass

if __name__ == '__main__':
    self_dir = os.path.dirname(__file__)

    # Example command lines:
    #   playnews ARRL
    #   playnews --split_silence ARRL
    #
    parser = ArgumentParser()
    parser.add_argument('--verbose', '-v', action='store_true',
        help='Turn on verbose logging')
    parser.add_argument('--debug', '-d', action='store_true',
        help='Turn on debugging logs')
    parser.add_argument('--config', '-c', metavar='FILE',
        default='{}/playnews.ini'.format(self_dir),
        help='Read configruation from FILE')
    parser.add_argument('--split-silence', '-S', action='store_true',
        help='Split the audio file at silence breaks')
    parser.add_argument('--split-time', '-s', metavar='SECS',
        help='Split the audio file by time')
    parser.add_argument('--no-play', '-x', action='store_true', default=False,
        help='Only fetch and split the audio feed')
    parser.add_argument('--node', '-n', type=int, metavar='NODE_NUM',
        help='Allstar node to send audio stream to')
    parser.add_argument('--global', '-g', action='store_true',
        help='Playback on all connected nodes')
    parser.add_argument('--when', '-t', type=str, metavar='TIME',
        help='Play audio stream at a specific time')
    parser.add_argument('--file', '-f', type=str, metavar='FILE',
        help='Use the specified file instead of downloading')
    parser.add_argument('feed', metavar='FEED_KEY',
        help='The playnews.ini section name for the audio feed')
    args = parser.parse_args()


    logging.debug("Reading config file: {}".format(args.config))
    conf = ConfigParser(interpolation=ExtendedInterpolation())
    conf.read(args.config)
    global_conf = ConfSettings(conf['global'])
    feed_conf = ConfSettings(conf[args.feed])

    # Set the logging settings from the config file
    log_levels={'DEBUG': logging.DEBUG, 'INFO': logging.INFO,
                'WARNING': logging.WARNING, 'WARN': logging.WARNING,
                'ERROR': logging.ERROR, 'ERR': logging.ERROR,
                'CRITICAL': logging.CRITICAL, 'CRIT': logging.CRITICAL,
                'FATAL': logging.CRITICAL}
    logging.basicConfig(file=global_conf.value('logfile', '/dev/tty'),
            encoding='utf-8',
            level=log_levels[global_conf.value('log_level', 'WARNING')])

    # override log level if switches are provided
    if args.verbose:
        logging.getLogger().setLevel(logging.INFO)
    if args.debug:
        logging.getLogger().setLevel(logging.DEBUG)

    # define a conveniece function to play files through asterisk
    play = lambda af: asterisk_play(os.path.splitext(af)[0],
                asterisk_prog=asterisk_prog(global_conf),
                mode=play_mode(global_conf, feed_conf),
                node_num=node_num(args.node, global_conf))

    # verify that required values are in the config file
    try:
        temp_dir = global_conf['download_dir']
        play_dir = global_conf['play_dir']

        # ensure that the temp_dir and play_dir exists
        if not os.path.isdir(temp_dir):
            os.makedirs(temp_dir)
        if not os.path.isdir(play_dir):
            os.makedirs(play_dir)

    except KeyError as e:
        logging.critical("Error in {} config file".format(args.config))
        logging.exception(e)
        sys.exit(1)

    # Ensure that the required programs are installed
    if not is_ffmpeg_installed():
        logging.critical("ffmpeg is not installed")
        sys.exit(1)
    if not is_sox_installed():
        logging.critical("sox is not installed")
        sys.exit(1)

    if feed_has_expired(args.feed, play_dir, feed_conf.value('expiration', None)):
        # retrieve a copy of the audio stream
        download_base_filename = os.path.join(temp_dir, next(temp_filename()))
        if args.file:
            # copy file to temp_dir
            ext = file_extension(args.file)
            downloaded_file = "{}.{}".format(download_base_filename, ext)
            shutil.copyfile(args.file, downloaded_file)
        else:
            download_url = find_audio_stream(args.feed, conf)
            verify_ssl = feed_conf.value('verify_ssl', True)
            verify_ssl = (verify_ssl == True or verify_ssl == 'true')
            downloaded_file = fetch_audio_file(download_url,
                    download_base_filename, verify=verify_ssl)
        logging.debug("Working copy of audio file: {}".format(downloaded_file))


        split_audio_file(downloaded_file, dest_dir=play_dir, nugget=args.feed,
            split_type='silence',
            silence_duration=feed_conf.value('split_silence', None),
            silence_thresh=feed_conf.value('silence_thresh', -16),
            ignore_short=feed_conf.value('ignore_short', 0))

    if not args.no_play:
        logging.info("Preparing to play audio segments")

        if args.when and args.when.lower() != 'now':
            now = datetime.now()
            play_time = date_parse(args.when)

            if 'announce_10min' in feed_conf.keys():
                while play_time - now > timedelta(minutes=10):
                    logging.debug('Sleeping for 1 minute')
                    time.sleep(60)
                    now = datetime.now()

                if play_time - now == timedelta(minutes=10):
                    logging.info('Playing 10 minute announcment')
                    play(os.path.join(self_dir, feed_conf['announce_10min']))

            if 'announce_5min' in feed_conf.keys():
                while play_time - now > timedelta(minutes=5):
                    logging.debug('Sleeping for 1 minute')
                    time.sleep(60)
                    now = datetime.now()

                if play_time - now == timedelta(minutes=5):
                    logging.info('Playing 5 minute announcement')
                    play(os.path.join(self_dir, feed_conf['announce_5min']))

            while play_time - now != timedelta(minutes=0):
                logging.debug('Sleeping for 1 minute')
                time.sleep(60)
                now = datetime.now()

        if 'announce_start' in feed_conf.keys():
            logging.info('Playing start announcement')
            play(os.path.join(self_dir, feed_conf['announce_start']))

        play_time = 0.0
        with open(os.path.join(play_dir, "{}-manifest.txt".format(args.feed)), 'r') as manifest:
            for line in manifest.readlines():
                audio_file, segment_time = line.strip().split(',')
                play_file = os.path.join(play_dir, audio_file)
                play_time += float(segment_time)

                logging.info("Playing {}".format(play_file))
                play(play_file)
                # need to sleep until the audio file finishes playing
                logging.debug("Sleeping for {} seconds".format(segment_time))
                time.sleep(float(segment_time))

                # if play_time is over 8 mins, then lets do station identification
                if global_conf['id_enable'] == 'true' and play_time >= id_time(global_conf):
                    send_station_id(global_conf['id_text'])
                    play_time = 0

                # delay for 'segment_delay'
                logging.debug("Delaying {} secs between segments".format(global_conf['segment_delay']))
                time.sleep(int(global_conf['segment_delay']))

        play(os.path.join(self_dir, feed_conf['announce_stop']))

        logging.info("Done playing {} feed".format(args.feed))
